import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:my_sushi/helper/color_helper.dart';

ThemeData lightTheme = ThemeData(
    brightness: Brightness.light,
    scaffoldBackgroundColor: ColorHelper.white,
    highlightColor: Colors.transparent,
    splashColor: Colors.transparent,
    textTheme: GoogleFonts.latoTextTheme().copyWith(
    ),
);
