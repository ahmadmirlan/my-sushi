import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:my_sushi/controller/order_controller.dart';
import 'package:my_sushi/helper/color_helper.dart';
import 'package:my_sushi/pages/order_page.dart';
import 'package:my_sushi/widgets/order/pay_order_widget.dart';

class OrderScreen extends StatelessWidget {
  const OrderScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Get.find<OrderController>();
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: ColorHelper.white,
        leading: Container(
          padding: EdgeInsets.only(left: Get.width * 0.02, top: 5, bottom: 5),
          child: IconButton(
            onPressed: () {
              Get.back();
            },
            icon: Icon(Icons.arrow_back_rounded,
                color: ColorHelper.dark, size: 25),
          ),
        ),
        centerTitle: true,
        title: Text('Checkout', style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.w600,
            color: ColorHelper.dark
        )),
      ),
      body: const OrderPage(),
      bottomNavigationBar: const PayOrderWidget(),
    );
  }
}
