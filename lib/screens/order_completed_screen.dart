import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:my_sushi/controller/order_controller.dart';
import 'package:my_sushi/helper/color_helper.dart';
import 'package:my_sushi/routes/AppRoutes.dart';
import 'package:my_sushi/widgets/default_loading_widget.dart';

class OrderCompletedScreen extends StatelessWidget {
  const OrderCompletedScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenWidth = Get.width;
    final orderController = Get.find<OrderController>();
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset('assets/icons/cheers.png', width: 100, height: 100)
            ],
          ),
          Container(
            margin: const EdgeInsets.only(top: 10, bottom: 20),
            child: Text('Yor order has been placed!',
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.w900, color: ColorHelper.dark)),
          ),
          Container(
            padding: EdgeInsets.only(
                left: screenWidth * 0.06, right: screenWidth * 0.06),
            child: const Text(
                'Please wait and Relax while we serving your order!',
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontStyle: FontStyle.italic,
                    fontWeight: FontWeight.w500,
                    fontSize: 18)),
          )
        ],
      ),
      floatingActionButton: Container(
        height: 60,
        width: screenWidth * 0.8,
        padding: const EdgeInsets.only(left: 30, right: 30),
        decoration: BoxDecoration(
            color: ColorHelper.primary,
            borderRadius: BorderRadius.circular(30)
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GestureDetector(
              onTap: ((){
                context.loaderOverlay
                    .show(widget: const DefaultLoadingWidget());
                Future.delayed(const Duration(seconds: 1), () {
                  orderController.resetOrder();
                  context.loaderOverlay.hide();
                  Get.toNamed(AppRoutes.MENU);
                });
              }),
              child: Text('Got It', style: TextStyle(
                  color: ColorHelper.white,
                  fontSize: 18,
                  fontWeight: FontWeight.w900
              )),
            ),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
