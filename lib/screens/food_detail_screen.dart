import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_sushi/helper/color_helper.dart';
import 'package:my_sushi/pages/food_detail_page.dart';
import 'package:my_sushi/widgets/order/food_order_widget.dart';

class FoodDetailScreen extends StatelessWidget {
  const FoodDetailScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: ColorHelper.whiteDarker,
        leading: Container(
          padding: EdgeInsets.only(left: Get.width * 0.02, top: 5, bottom: 5),
          child: IconButton(
            onPressed: () {
              Get.back();
            },
            icon: Icon(Icons.arrow_back_rounded,
                color: ColorHelper.dark, size: 25),
          ),
        ),
        actions: [
          Container(
            padding: EdgeInsets.only(right: Get.width * 0.03),
            child: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: Icon(Icons.more_vert,
                  color: ColorHelper.dark, size: 25),
            ),
          )
        ],
      ),
      body: const FoodDetailPage(),
      bottomNavigationBar: const FoodOrderWidget(),
    );
  }
}
