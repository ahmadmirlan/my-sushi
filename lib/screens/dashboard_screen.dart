import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_sushi/helper/color_helper.dart';
import 'package:my_sushi/pages/dashboard_page.dart';
import 'package:my_sushi/widgets/menu/menu_bar_widget.dart';

class DashboardScreen extends StatelessWidget {
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: ColorHelper.white,
          leading: Container(
              padding: EdgeInsets.only(left: Get.width * 0.03),
              child: Center(
                child: Text('Hello, Mirlan',
                    style: TextStyle(
                        color: ColorHelper.dark,
                        fontSize: 18,
                        fontWeight: FontWeight.w600)),
              )),
          leadingWidth: 120,
          actions: [
            Container(
                width: 40,
                margin: EdgeInsets.only(right: Get.width * 0.04),
                padding: const EdgeInsets.all(5),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(color: ColorHelper.dark)),
                child: Image.asset('assets/icons/leonardo.png'))
          ],
        ),
        body: const DashboardPage(),
        bottomNavigationBar: const MenuBarWidget()
    );
  }
}
