import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:my_sushi/helper/color_helper.dart';
import 'package:my_sushi/routes/AppRoutes.dart';

class EmptyOrderWidget extends StatelessWidget {
  const EmptyOrderWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
          left: Get.width * 0.03,
          right: Get.width * 0.03,
          top: Get.height * 0.2),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Center(
            child: Image.asset('assets/icons/confused.png',
                width: Get.width * 0.5),
          ),
          Text('You are not ordering anything yet Buddy',
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: ColorHelper.dark,
                  fontSize: 20,
                  fontWeight: FontWeight.w600)),
          GestureDetector(
            onTap: () {
              Get.toNamed(AppRoutes.MENU);
            },
            child: Container(
              width: Get.width * 0.70,
              height: Get.height * 0.06,
              margin: EdgeInsets.only(top: Get.height * 0.02),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: ColorHelper.primary),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('Order Now',
                      style: TextStyle(
                          color: ColorHelper.white,
                          fontSize: 18,
                          fontWeight: FontWeight.w400)),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
