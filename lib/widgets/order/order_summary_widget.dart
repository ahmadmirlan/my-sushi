import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_sushi/controller/order_controller.dart';
import 'package:my_sushi/helper/color_helper.dart';
import 'package:my_sushi/models/model.dart';

class OrderSummaryWidget extends StatelessWidget {
  final List<OrderedMenuModel> orderedMenus;

  const OrderSummaryWidget({Key? key, required this.orderedMenus})
      : super(key: key);

  @override
  Widget build(Object context) {
    final controller = Get.find<OrderController>();
    return Container(
      padding: EdgeInsets.all(Get.width * 0.05),
      decoration: BoxDecoration(
          color: ColorHelper.whiteDarker,
          borderRadius: BorderRadius.circular(10)),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Subtotal',
                  style: TextStyle(
                      fontWeight: FontWeight.w500,
                      color: ColorHelper.grey,
                      fontSize: 16)),
              Text('\$ ${controller.countSubTotal.toStringAsFixed(2)}',
                  style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: ColorHelper.dark,
                      fontSize: 16)),
            ],
          ),
          Container(
            margin: EdgeInsets.only(
                top: Get.height * 0.015, bottom: Get.height * 0.015),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('PPN 11%',
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        color: ColorHelper.grey,
                        fontSize: 16)),
                Text('\$ ${controller.countTax.toStringAsFixed(2)}',
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        color: ColorHelper.dark,
                        fontSize: 16)),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Delivery Fee',
                  style: TextStyle(
                      fontWeight: FontWeight.w500,
                      color: ColorHelper.grey,
                      fontSize: 16)),
              Text('\$ ${controller.deliveryFee}',
                  style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: ColorHelper.dark,
                      fontSize: 16)),
            ],
          ),
        ],
      ),
    );
  }
}
