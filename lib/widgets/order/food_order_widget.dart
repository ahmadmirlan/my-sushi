import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:my_sushi/controller/menu_controller.dart';
import 'package:my_sushi/controller/order_controller.dart';
import 'package:my_sushi/helper/color_helper.dart';
import 'package:my_sushi/routes/AppRoutes.dart';
import 'package:my_sushi/widgets/default_loading_widget.dart';

class FoodOrderWidget extends StatelessWidget {
  const FoodOrderWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final menuController = Get.find<MenuController>();
    return GetBuilder<OrderController>(builder: (controller) {
      return Container(
        height: Get.height * 0.1,
        padding:
            EdgeInsets.only(left: Get.width * 0.05, right: Get.width * 0.05),
        decoration: BoxDecoration(color: ColorHelper.white),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            GestureDetector(
              onTap: () {
                controller.addMenuToList(menuController.selectedFood);
                Get.toNamed(AppRoutes.ORDER);
              },
              child: Container(
                width: Get.width * 0.70,
                height: Get.height * 0.06,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: ColorHelper.primary),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('Checkout',
                        style: TextStyle(
                            color: ColorHelper.white,
                            fontSize: 18,
                            fontWeight: FontWeight.w400)),
                  ],
                ),
              ),
            ),
            GestureDetector(
                onTap: () {
                  context.loaderOverlay.show(widget: const DefaultLoadingWidget());
                  Future.delayed(const Duration(milliseconds: 500), () {
                    controller.addMenuToList(menuController.selectedFood);
                    final snackBar = SnackBar(
                      backgroundColor: ColorHelper.dark,
                      content: Text(
                          '${menuController.selectedFood.name} has been added to the cart!',
                          style: GoogleFonts.poppins(
                              color: ColorHelper.white,
                              fontSize: 20,
                              fontWeight: FontWeight.w500)),
                    );
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                    context.loaderOverlay.hide();
                  });
                },
                child: Container(
                  height: Get.height * 0.06,
                  width: Get.height * 0.06,
                  margin: EdgeInsets.only(left: Get.width * 0.05),
                  decoration: BoxDecoration(
                    color: ColorHelper.secondary,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Icon(
                    Icons.shopping_bag_outlined,
                    color: ColorHelper.primary,
                  ),
                ))
          ],
        ),
      );
    });
  }
}
