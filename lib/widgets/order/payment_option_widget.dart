import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:my_sushi/controller/payment_controller.dart';
import 'package:my_sushi/helper/color_helper.dart';
import 'package:my_sushi/routes/AppRoutes.dart';
import 'package:my_sushi/widgets/default_loading_widget.dart';

class PaymentOptionWidget extends StatelessWidget {
  const PaymentOptionWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<PaymentController>(builder: (controller) {
      return Container(
        decoration: BoxDecoration(
          color: ColorHelper.white,
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(
                height: (56 * 6).toDouble(),
                child: Container(
                    decoration: BoxDecoration(
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20),
                      ),
                      color: ColorHelper.white,
                    ),
                    child: Stack(
                      alignment: const Alignment(0, 0),
                      children: <Widget>[
                        Positioned(
                          top: 10,
                          child: Center(
                            child: Text('Pay With:',
                                style: TextStyle(
                                    color: ColorHelper.dark,
                                    fontSize: 22,
                                    fontWeight: FontWeight.bold)),
                          ),
                        ),
                        Positioned(
                          child: Container(
                            margin: EdgeInsets.only(top: Get.height * 0.05),
                            child: ListView(
                                physics: const NeverScrollableScrollPhysics(),
                                children: List.generate(
                                    controller.availablePayment.length,
                                    (index) {
                                  return GestureDetector(
                                    onTap: () {
                                      controller.changePayment(
                                          controller.availablePayment[index]);
                                    },
                                    child: Container(
                                      padding: EdgeInsets.only(
                                          left: Get.width * 0.05,
                                          right: Get.width * 0.05),
                                      margin: EdgeInsets.only(
                                          top: Get.height * 0.015),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Image.asset(
                                            'assets/icons/${controller.availablePayment[index].icon}',
                                            height: 50,
                                          ),
                                          Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              Center(
                                                child: Text(
                                                    controller
                                                        .availablePayment[index]
                                                        .name,
                                                    style: TextStyle(
                                                        color: ColorHelper.dark,
                                                        fontSize: 18,
                                                        fontWeight:
                                                            FontWeight.bold)),
                                              ),
                                              Container(
                                                width: 20,
                                                height: 20,
                                                padding:
                                                    const EdgeInsets.all(1.5),
                                                margin: EdgeInsets.only(
                                                    left: Get.width * 0.05),
                                                decoration: BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    border: Border.all(
                                                        color:
                                                            ColorHelper.dark)),
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                      shape: BoxShape.circle,
                                                      color: controller
                                                                  .availablePayment[
                                                                      index]
                                                                  .id ==
                                                              controller
                                                                  .selectedPayment
                                                                  .id
                                                          ? ColorHelper.dark
                                                          : ColorHelper.white),
                                                ),
                                              )
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                  );
                                })),
                          ),
                        )
                      ],
                    ))),
            Container(
              height: 90,
              color: ColorHelper.white,
              padding: EdgeInsets.only(bottom: Get.height * 0.035),
              child: GestureDetector(
                onTap: () {
                  context.loaderOverlay
                      .show(widget: const DefaultLoadingWidget());
                  Future.delayed(const Duration(seconds: 1), () {
                    Get.toNamed(AppRoutes.ORDER_COMPLETED);
                    context.loaderOverlay.hide();
                  });
                },
                child: Container(
                  width: Get.width * 0.60,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(color: ColorHelper.primary, width: 3)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('Pay Now',
                          style: TextStyle(
                              color: ColorHelper.primary,
                              fontSize: 18,
                              fontWeight: FontWeight.w600)),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      );
    });
  }
}
