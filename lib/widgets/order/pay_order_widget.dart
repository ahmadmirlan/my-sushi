import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_sushi/controller/order_controller.dart';
import 'package:my_sushi/controller/payment_controller.dart';
import 'package:my_sushi/helper/color_helper.dart';
import 'package:my_sushi/widgets/order/payment_option_widget.dart';

class PayOrderWidget extends StatelessWidget {
  const PayOrderWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    showMenu() {
      Get.lazyPut<PaymentController>(() => PaymentController());
      showModalBottomSheet(
          backgroundColor: Colors.transparent,
          context: context,
          builder: (BuildContext context) {
            return const PaymentOptionWidget();
          });
    }

    return GetBuilder<OrderController>(builder: (controller) {
      return controller.getOrderedMenu.isNotEmpty
          ? Container(
              padding: EdgeInsets.only(
                  left: Get.width * 0.05,
                  right: Get.width * 0.05,
                  top: Get.height * 0.01),
              height: 100,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Total',
                          style: TextStyle(
                              fontWeight: FontWeight.w900,
                              color: ColorHelper.dark,
                              fontSize: 20)),
                      Container(
                        margin: EdgeInsets.only(top: Get.height * 0.015),
                        child: Text(
                            '\$ ${controller.grandTotal.toStringAsFixed(2)}',
                            style: TextStyle(
                                fontWeight: FontWeight.w900,
                                color: ColorHelper.dark,
                                fontSize: 25)),
                      ),
                    ],
                  ),
                  GestureDetector(
                    onTap: () {
                      showMenu();
                    },
                    child: Container(
                      width: Get.width * 0.60,
                      height: Get.height * 0.07,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: ColorHelper.primary),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text('Pay Now',
                              style: TextStyle(
                                  color: ColorHelper.white,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w600)),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            )
          : const SizedBox();
    });
  }
}
