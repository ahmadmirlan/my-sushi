import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_sushi/controller/order_controller.dart';
import 'package:my_sushi/helper/color_helper.dart';
import 'package:my_sushi/routes/AppRoutes.dart';

class MenuBarWidget extends StatelessWidget {
  const MenuBarWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<OrderController>(builder: (controller) {
      return Container(
        padding:
            EdgeInsets.only(left: Get.width * 0.06, right: Get.width * 0.06),
        decoration: BoxDecoration(
            color: ColorHelper.white,
            border: Border(
                top: BorderSide(
                    color: ColorHelper.grey.withOpacity(0.6), width: 0.2))),
        height: 90,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.home_rounded,
                  color: ColorHelper.primary,
                  size: 30,
                )
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.discount_outlined,
                  color: ColorHelper.grey,
                  size: 30,
                )
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: () {
                    Get.toNamed(AppRoutes.ORDER);
                  },
                  child: Badge(
                    badgeColor: ColorHelper.primary,
                    badgeContent: Text('${controller.getOrderedMenu.length}',
                        style: TextStyle(color: ColorHelper.white)),
                    child: Icon(
                      Icons.local_grocery_store_outlined,
                      color: ColorHelper.grey,
                      size: 30,
                    ),
                  ),
                )
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.person_outline,
                  color: ColorHelper.grey,
                  size: 30,
                )
              ],
            ),
          ],
        ),
      );
    });
  }
}
