import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_sushi/helper/color_helper.dart';
import 'package:my_sushi/models/model.dart';

class IngredientWidget extends StatelessWidget {
  final IngredientsModel ingredient;

  const IngredientWidget({Key? key, required this.ingredient})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: Get.width * 0.05),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: Get.height * 0.01),
            padding: EdgeInsets.only(
                top: Get.width * 0.03,
                bottom: Get.width * 0.03,
                left: Get.width * 0.03,
                right: Get.width * 0.03),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: ColorHelper.whiteDarker,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Image.asset('assets/icons/${ingredient.icon}', width: Get.width * 0.1,),
              ],
            ),
          ),
          Text(ingredient.name, style: TextStyle(
              fontSize: 14,
              color: ColorHelper.grey,
            fontWeight: FontWeight.w600
          ))
        ],
      ),
    );
  }
}
