import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:my_sushi/controller/order_controller.dart';
import 'package:my_sushi/helper/color_helper.dart';
import 'package:my_sushi/models/model.dart';

class FoodCardWidget extends StatelessWidget {
  final MenuModel food;

  const FoodCardWidget({Key? key, required this.food}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final width = Get.width;
    final height = Get.height;
    return GetBuilder<OrderController>(builder: (controller) {
      final isOrdered = controller.isMenuHaveAdded(food);
      return Container(
        padding: EdgeInsets.only(top: width * 0.03, bottom: width * 0.03),
        margin: EdgeInsets.only(left: width * 0.05),
        width: width * 0.42,
        decoration: BoxDecoration(
            color: ColorHelper.whiteDarker,
            borderRadius: BorderRadius.circular(20)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.only(left: width * 0.03, right: width * 0.03),
              child: Row(
                children: [
                  Icon(Icons.star_rounded, color: ColorHelper.yellow),
                  Text(food.ratings.toString(),
                      style: TextStyle(
                          color: ColorHelper.grey,
                          fontWeight: FontWeight.bold,
                          fontSize: 15))
                ],
              ),
            ),
            Center(
              child: Image.asset('assets/img/${food.icon}',
                  height: width * 0.3, fit: BoxFit.cover),
            ),
            Container(
              margin: EdgeInsets.only(top: height * 0.01),
              padding: EdgeInsets.only(left: width * 0.03, right: width * 0.03),
              child: Text(food.name,
                  style: TextStyle(
                      fontWeight: FontWeight.w900,
                      fontSize: 16,
                      color: ColorHelper.dark)),
            ),
            Container(
              padding: EdgeInsets.only(left: width * 0.03, right: width * 0.03),
              child: Row(
                children: List.generate(food.mainIngredients.length, (index) {
                  return Text('${food.mainIngredients[index].name}, ',
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 11,
                          color: ColorHelper.grey));
                }),
              ),
            ),
            Container(
                padding:
                    EdgeInsets.only(left: width * 0.03, right: width * 0.03),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('\$ ${food.price.toString()}',
                        style: GoogleFonts.concertOne(
                            fontSize: 20, fontWeight: FontWeight.w500)),
                    Container(
                      padding: const EdgeInsets.all(2),
                      decoration: BoxDecoration(
                          color: isOrdered
                              ? ColorHelper.whiteDarker
                              : ColorHelper.secondary,
                          borderRadius: BorderRadius.circular(5)),
                      child: isOrdered
                          ? Icon(
                              Icons.done,
                              color: ColorHelper.dark,
                            )
                          : GestureDetector(
                              onTap: () {
                                controller.addMenuToList(food);
                              },
                              child: Icon(
                                Icons.add,
                                color: ColorHelper.primary,
                              ),
                            ),
                    )
                  ],
                ))
          ],
        ),
      );
    });
  }
}
