import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:my_sushi/helper/color_helper.dart';

class TaglineWidget extends StatelessWidget {
  const TaglineWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Container(
                margin: EdgeInsets.only(right: Get.width * 0.03),
                child: Text('Enjoy',
                    style: GoogleFonts.anton(
                        fontSize: 32,
                        fontWeight: FontWeight.w500,
                        color: ColorHelper.dark)),
              ),
              Text('Japanese',
                  style: GoogleFonts.anton(
                      fontSize: 30,
                      fontWeight: FontWeight.w500,
                      color: ColorHelper.primary)),
            ],
          ),
          Text('Taste in Your Town',
              style: GoogleFonts.anton(
                  fontSize: 30,
                  fontWeight: FontWeight.w500,
                  color: ColorHelper.dark))
        ],
      ),
    );
  }
}
