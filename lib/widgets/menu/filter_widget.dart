import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_sushi/helper/color_helper.dart';
import 'package:my_sushi/models/model.dart';

class FilterCategoriesWidget extends StatelessWidget {
  final bool isSelected;
  final CategoryModel filter;

  const FilterCategoriesWidget(
      {Key? key, required this.isSelected, required this.filter})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenWidth = Get.width;
    final screenHeight = Get.width;

    return Center(
      child: Container(
        margin: EdgeInsets.only(left: screenWidth * 0.05),
        width: 120,
        child: Container(
          padding: EdgeInsets.only(
              left: screenHeight * 0.02,
              right: screenHeight * 0.02,
              top: screenHeight * 0.03,
              bottom: screenHeight * 0.03),
          decoration: BoxDecoration(
              color: isSelected ? ColorHelper.primary : ColorHelper.whiteDarker,
              borderRadius: BorderRadius.circular(5)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Image.asset('assets/icons/${filter.icon}', height: 25, width: 25),
              Text(filter.categoryName,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color:
                          isSelected ? ColorHelper.white : ColorHelper.dark)),
            ],
          ),
        ),
      ),
    );
  }
}
