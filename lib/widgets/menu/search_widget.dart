import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_sushi/helper/color_helper.dart';

class SearchWidget extends StatelessWidget {
  const SearchWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 60,
      child: TextField(
        decoration: InputDecoration(
            contentPadding:
                const EdgeInsets.only(left: 15, right: 15, top: 20, bottom: 20),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(color: ColorHelper.whiteDarker, width: 2.5)),
            filled: true,
            fillColor: ColorHelper.white,
            hintText: 'Find your meal...',
            prefixIcon: Icon(Icons.search, color: ColorHelper.grey, size: 25),
            hintStyle: TextStyle(
                fontWeight: FontWeight.w500, color: ColorHelper.grey)),
      ),
    );
  }
}
