import 'package:get/get.dart';
import 'package:my_sushi/controller/menu_binding.dart';
import 'package:my_sushi/controller/order_binding.dart';
import 'package:my_sushi/screens/dashboard_screen.dart';
import 'package:my_sushi/screens/food_detail_screen.dart';
import 'package:my_sushi/screens/order_completed_screen.dart';
import 'package:my_sushi/screens/order_screen.dart';
import 'AppRoutes.dart';

class AppPages {
  static var list = [
    GetPage(
        name: AppRoutes.MENU,
        page: () => const DashboardScreen(),
        bindings: [MenuBinding(), OrderBinding()]),
    GetPage(
        name: AppRoutes.MENU_DETAIL,
        page: () => const FoodDetailScreen(),
        bindings: [MenuBinding(), OrderBinding()]),
    GetPage(
        name: AppRoutes.ORDER,
        page: () => const OrderScreen(),
        bindings: [OrderBinding()]),
    GetPage(
        name: AppRoutes.ORDER_COMPLETED,
        page: () => const OrderCompletedScreen()),
  ];
}
