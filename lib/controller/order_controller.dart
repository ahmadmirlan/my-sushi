import 'package:get/get.dart';
import 'package:my_sushi/data/count_total_helper.dart';
import 'package:my_sushi/models/model.dart';

class OrderController extends GetxController {
  List<OrderedMenuModel> _orderedMenu = [];

  List<OrderedMenuModel> get getOrderedMenu => _orderedMenu;

  double get countSubTotal => CountTotalHelper.countSubTotal(_orderedMenu);

  double get countTax => CountTotalHelper.countPPN(countSubTotal);

  double get deliveryFee => 1.5;

  double get grandTotal => countSubTotal + countTax + deliveryFee;

  void addMenuToList(MenuModel menu) {
    OrderedMenuModel? isExist = _orderedMenu.firstWhereOrNull((element) {
      return element.menu.id == menu.id;
    });

    if (isExist == null) {
      _orderedMenu.add(OrderedMenuModel(menu, 1));
      update();
    }
  }

  void addOrderedMenu(MenuModel menu) {
    var currentOrder = _orderedMenu.indexWhere((element) {
      return element.menu.id == menu.id;
    });

    if (currentOrder > -1) {
      _orderedMenu[currentOrder].quantity += 1;
      update();
      return;
    }
    _orderedMenu.add(OrderedMenuModel(menu, 1));
    update();
  }

  void removeOrderedMenu(MenuModel menu) {
    var currentOrder = _orderedMenu.indexWhere((element) {
      return element.menu.id == menu.id;
    });

    if (currentOrder < 0) {
      return;
    }

    if (_orderedMenu[currentOrder].quantity < 2) {
      _orderedMenu.removeAt(currentOrder);
      update();
      return;
    }

    _orderedMenu[currentOrder].quantity -= 1;
    update();
  }

  void resetOrder() {
    _orderedMenu = [];
    update();
  }

  bool isMenuHaveAdded(MenuModel menu) {
    OrderedMenuModel? isExist = _orderedMenu.firstWhereOrNull((element) {
      return element.menu.id == menu.id;
    });

    return isExist != null;
  }
}
