import 'package:get/get.dart';
import 'package:my_sushi/data/dummy_data.dart';
import 'package:my_sushi/models/model.dart';

class PaymentController extends GetxController {
  PaymentModel _selectedPayment = DummyData.payments[0];
  List<PaymentModel> _availablePayment = [];

  PaymentModel get selectedPayment => _selectedPayment;

  List<PaymentModel> get availablePayment => _availablePayment;

  @override
  void onInit() {
    super.onInit();
    _selectedPayment = DummyData.payments[0];
    _availablePayment = DummyData.payments;
  }

  void changePayment(PaymentModel payment) {
    _selectedPayment = payment;
    update();
  }
}
