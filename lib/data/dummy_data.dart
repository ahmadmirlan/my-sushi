import 'package:my_sushi/models/model.dart';

class DummyData {
  static List<MenuModel> foods = [
    MenuModel(1, 'Sushi', 'sushi_1.png', 12.5, categories[0], ingredients, 4.8,
        [ingredients[1], ingredients[3], ingredients[5]]),
    MenuModel(2, 'Original Sushi', 'sushi0.png', 10.3, categories[0],
        ingredients, 4.5, [ingredients[1], ingredients[3], ingredients[5]]),
    MenuModel(3, 'Kyoto Super Set', 'sushi2.png', 12.5, categories[0],
        ingredients, 5.0, [ingredients[1], ingredients[3], ingredients[5]]),
    MenuModel(4, 'Kakumi Special', 'sushi4.png', 7.2, categories[0],
        ingredients, 4.6, [ingredients[1], ingredients[3], ingredients[5]]),
    MenuModel(5, 'Colourfull Sushi', 'sushi5.png', 9.2, categories[0],
        ingredients, 4.9, [ingredients[1], ingredients[3], ingredients[5]]),
    MenuModel(6, 'All Sushi Packages', 'sushi3.png', 12.2, categories[0],
        ingredients, 4.7, [ingredients[1], ingredients[3], ingredients[5]]),
    MenuModel(7, 'Sushi Roll California', 'sushi6.png', 11.2, categories[0],
        ingredients, 4.2, [ingredients[1], ingredients[3], ingredients[5]]),
    MenuModel(8, 'Nasu', 'tempura1.png', 11.2, categories[1], ingredients, 4.9,
        [ingredients[1], ingredients[3], ingredients[5]]),
    MenuModel(9, 'Kabocha', 'tempura2.png', 11.2, categories[1], ingredients,
        4.3, [ingredients[1], ingredients[3], ingredients[5]]),
    MenuModel(10, 'Shiitake', 'tempura3.png', 11.2, categories[1], ingredients,
        4.5, [ingredients[1], ingredients[3], ingredients[5]]),
    MenuModel(11, 'Ninjin', 'tempura4.png', 11.2, categories[1], ingredients,
        4.8, [ingredients[1], ingredients[3], ingredients[5]]),
    MenuModel(12, 'Coca Cola', 'cola.png', 0.20, categories[2], drinksRecipe,
        5.0, [drinksRecipe[0], drinksRecipe[1], drinksRecipe[2]]),
    MenuModel(13, 'Sprite', 'sprite.png', 0.20, categories[2], drinksRecipe,
        5.0, [drinksRecipe[3], drinksRecipe[1], drinksRecipe[2]]),
    MenuModel(12, 'Pepsi', 'pepsi.png', 0.25, categories[2], drinksRecipe, 5.0,
        [drinksRecipe[0], drinksRecipe[1], drinksRecipe[2]]),
    MenuModel(12, 'Fanta', 'fanta.png', 0.20, categories[2], drinksRecipe, 5.0,
        [drinksRecipe[0], drinksRecipe[1], drinksRecipe[2]]),
  ];

  static List<CategoryModel> categories = [
    CategoryModel(1, 'Sushi', 'sushi.png'),
    CategoryModel(2, 'Tempura', 'tempura.png'),
    CategoryModel(3, 'Soft Drink', 'cola.png'),
  ];

  static List<IngredientsModel> ingredients = [
    IngredientsModel(1, 'Rice', 'rice.png'),
    IngredientsModel(2, 'Tuna', 'tuna.png'),
    IngredientsModel(3, 'Salmon', 'salmon.png'),
    IngredientsModel(4, 'Ebi', 'ebi.png'),
    IngredientsModel(5, 'Mushroom', 'mushroom.png'),
    IngredientsModel(6, 'Cheese', 'cheese.png'),
  ];

  static List<IngredientsModel> drinksRecipe = [
    IngredientsModel(1, 'Cola', 'can.png'),
    IngredientsModel(2, 'Sugar', 'sugar.png'),
    IngredientsModel(3, 'Water', 'water.png'),
    IngredientsModel(4, 'Lime', 'lime.png'),
  ];

  static List<PaymentModel> payments = [
    PaymentModel(1, 'Gopay', 'gopay.png'),
    PaymentModel(2, 'Shopee Pay', 'shopee.png'),
    PaymentModel(3, 'Paypal', 'paypal.png'),
    PaymentModel(4, 'Credit Card', 'credit.png'),
  ];

  static String descriptionText =
      "We want to be your partner for all your food and drink needs. We offer a huge selection of fresh food products for your everyday life, including meat, fresh fish, fruit and vegetables as well as dairy products and snacks.";
}
