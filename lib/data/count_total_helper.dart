import 'package:my_sushi/models/model.dart';

class CountTotalHelper {
  static double countSubTotal(List<OrderedMenuModel> orderedMenu) {
    var countTotal = 0.0;
    for (var element in orderedMenu) {
      final total = element.quantity * element.menu.price;
      countTotal = countTotal + total;
    }
    return countTotal;
  }

  static double countPPN(double total) {
    return total * 0.11;
  }
}
