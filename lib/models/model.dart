class MenuModel {
  int id;
  String name;
  String icon;
  double price;
  CategoryModel category;
  List<IngredientsModel> ingredients;
  double ratings;
  List<IngredientsModel> mainIngredients;

  MenuModel(this.id, this.name, this.icon, this.price, this.category,
      this.ingredients, this.ratings, this.mainIngredients);
}

class CategoryModel {
  int id;
  String categoryName;
  String icon;

  CategoryModel(this.id, this.categoryName, this.icon);
}

class IngredientsModel {
  int id;
  String name;
  String icon;

  IngredientsModel(this.id, this.name, this.icon);
}

class OrderedMenuModel {
  late MenuModel menu;
  late int quantity;

  OrderedMenuModel(this.menu, this.quantity);
}

class PaymentModel {
  int id;
  String name;
  String icon;

  PaymentModel(this.id, this.name, this.icon);
}
