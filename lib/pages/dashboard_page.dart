import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:my_sushi/controller/menu_controller.dart';
import 'package:my_sushi/helper/color_helper.dart';
import 'package:my_sushi/routes/AppRoutes.dart';
import 'package:my_sushi/widgets/default_loading_widget.dart';
import 'package:my_sushi/widgets/menu/filter_widget.dart';
import 'package:my_sushi/widgets/menu/food_card_widget.dart';
import 'package:my_sushi/widgets/menu/search_widget.dart';
import 'package:my_sushi/widgets/menu/tagline_widget.dart';

class DashboardPage extends StatelessWidget {
  const DashboardPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<MenuController>(builder: (controller) {
      return Obx(() => controller.isLoading
          ? const DefaultLoadingWidget()
          : ListView(
              physics: const BouncingScrollPhysics(),
              children: [
                Container(
                  padding: EdgeInsets.only(
                      left: Get.width * 0.05, right: Get.width * 0.05),
                  margin: EdgeInsets.only(top: Get.height * 0.02),
                  child: const TaglineWidget(),
                ),
                Container(
                  padding: EdgeInsets.only(
                      left: Get.width * 0.05, right: Get.width * 0.05),
                  margin: EdgeInsets.only(
                      bottom: Get.height * 0.03, top: Get.height * 0.03),
                  child: const SearchWidget(),
                ),
                Container(
                  padding: EdgeInsets.only(
                      left: Get.width * 0.055, right: Get.width * 0.055),
                  margin: EdgeInsets.only(bottom: Get.height * 0.02),
                  child: Text('Category',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          color: ColorHelper.dark,
                          fontSize: 20,
                          fontWeight: FontWeight.w900)),
                ),
                Container(
                  height: 50,
                  margin: EdgeInsets.only(bottom: Get.height * 0.03),
                  child: ListView(
                    physics: const BouncingScrollPhysics(),
                    scrollDirection: Axis.horizontal,
                    children: List.generate(controller.getAllCategories.length,
                        (index) {
                      return GestureDetector(
                        onTap: () {
                          if (controller.selectedCategory.id ==
                              controller.getAllCategories[index].id) {
                            return;
                          }
                          context.loaderOverlay
                              .show(widget: const DefaultLoadingWidget());
                          Future.delayed(const Duration(seconds: 1), () {
                            controller.setSelectedCategory(
                                controller.getAllCategories[index]);
                            context.loaderOverlay.hide();
                          });
                        },
                        child: FilterCategoriesWidget(
                            isSelected: controller.getAllCategories[index].id ==
                                controller.selectedCategory.id,
                            filter: controller.getAllCategories[index]),
                      );
                    }),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(
                      left: Get.width * 0.055, right: Get.width * 0.055),
                  margin: EdgeInsets.only(bottom: Get.height * 0.02),
                  child: Text('Populars',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          color: ColorHelper.dark,
                          fontSize: 20,
                          fontWeight: FontWeight.w900)),
                ),
                SizedBox(
                  height: Get.height * 0.32,
                  child: ListView(
                    physics: const BouncingScrollPhysics(),
                    scrollDirection: Axis.horizontal,
                    children:
                        List.generate(controller.getAllFoods.length, (index) {
                      return GestureDetector(
                        onTap: () {
                          controller
                              .setSelectedFood(controller.getAllFoods[index]);
                          context.loaderOverlay
                              .show(widget: const DefaultLoadingWidget());
                          Future.delayed(const Duration(seconds: 1), () {
                            Get.toNamed(AppRoutes.MENU_DETAIL);
                            context.loaderOverlay.hide();
                          });
                        },
                        child:
                            FoodCardWidget(food: controller.getAllFoods[index]),
                      );
                    }),
                  ),
                ),
              ],
            ));
    });
  }
}
