import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:my_sushi/controller/menu_controller.dart';
import 'package:my_sushi/data/dummy_data.dart';
import 'package:my_sushi/helper/color_helper.dart';
import 'package:my_sushi/widgets/menu/ingredient_widget.dart';

class FoodDetailPage extends StatelessWidget {
  const FoodDetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<MenuController>(builder: (controller) {
      final food = controller.selectedFood;
      final width = Get.width;
      final height = Get.height;
      return Container(
        decoration: BoxDecoration(color: ColorHelper.white),
        child: ListView(
          physics: const BouncingScrollPhysics(),
          children: [
            Container(
              color: ColorHelper.whiteDarker,
              child: Center(
                child: Image.asset(
                  'assets/img/${food.icon}',
                  height: height * 0.38,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: width * 0.05, right: width * 0.05),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: height * 0.015),
                        child: Text(food.name,
                            style: const TextStyle(
                                fontWeight: FontWeight.w900, fontSize: 20)),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            top: Get.height * 0.01, bottom: Get.height * 0.01),
                        child: Row(
                          children:
                              List.generate(food.mainIngredients.length, (index) {
                            return Text('${food.mainIngredients[index].name}, ',
                                style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 14,
                                    color: ColorHelper.grey));
                          }),
                        ),
                      ),
                      Row(
                        children: [
                          Icon(Icons.star_rounded, color: ColorHelper.yellow),
                          Text(food.ratings.toString(),
                              style: TextStyle(
                                  color: ColorHelper.grey,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15))
                        ],
                      ),
                    ],
                  ),
                  Text('\$ ${food.price}',
                      textAlign: TextAlign.start,
                      style: GoogleFonts.concertOne(
                          color: ColorHelper.dark,
                          fontSize: 25,
                          fontWeight: FontWeight.w900))
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: width * 0.05, right: width * 0.05),
              margin:
                  EdgeInsets.only(top: height * 0.04, bottom: height * 0.01),
              child: Text('Description',
                  style: TextStyle(
                      color: ColorHelper.dark.withOpacity(0.8),
                      fontWeight: FontWeight.w600,
                      fontSize: 18)),
            ),
            Container(
              padding: EdgeInsets.only(left: width * 0.05, right: width * 0.05),
              margin: EdgeInsets.only(bottom: height * 0.03),
              child: Text(DummyData.descriptionText,
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      color: ColorHelper.dark.withOpacity(0.8),
                      fontWeight: FontWeight.w400,
                      fontSize: 16)),
            ),
            Container(
              padding: EdgeInsets.only(left: width * 0.05, right: width * 0.05),
              margin: EdgeInsets.only(bottom: height * 0.02),
              child: Text('Ingredients',
                  style: TextStyle(
                      color: ColorHelper.dark.withOpacity(0.8),
                      fontWeight: FontWeight.w600,
                      fontSize: 18)),
            ),
            SizedBox(
              height: height * 0.15,
              width: height * 0.08,
              child: ListView(
                scrollDirection: Axis.horizontal,
                physics: const BouncingScrollPhysics(),
                children: List.generate(food.ingredients.length, (index) {
                  return IngredientWidget(ingredient: food.ingredients[index]);
                }),
              ),
            ),
          ],
        ),
      );
    });
  }
}
