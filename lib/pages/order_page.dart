import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:my_sushi/controller/order_controller.dart';
import 'package:my_sushi/helper/color_helper.dart';
import 'package:my_sushi/widgets/order/add_note_widget.dart';
import 'package:my_sushi/widgets/order/empty_order_widget.dart';
import 'package:my_sushi/widgets/order/order_summary_widget.dart';

class OrderPage extends StatelessWidget {
  const OrderPage({Key? key}) : super(key: key);

  Widget orderList() {
    return GetBuilder<OrderController>(builder: (controller) {
      return ListView(physics: const BouncingScrollPhysics(), children: [
        ...List.generate(controller.getOrderedMenu.length, (index) {
          return Container(
            margin: EdgeInsets.only(bottom: Get.height * 0.015),
            child: Row(
              children: [
                Container(
                  padding: const EdgeInsets.all(5),
                  margin: EdgeInsets.only(right: Get.width * 0.03),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: ColorHelper.whiteDarker,
                  ),
                  child: Image.asset(
                      'assets/img/${controller.getOrderedMenu[index].menu.icon}',
                      height: Get.width * 0.25,
                      width: Get.width * 0.25,
                      fit: BoxFit.cover),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(controller.getOrderedMenu[index].menu.name,
                        style: TextStyle(
                            color: ColorHelper.dark,
                            fontWeight: FontWeight.w600,
                            fontSize: 16)),
                    Container(
                      margin: const EdgeInsets.only(top: 10),
                      width: Get.width * 0.6,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                  '\$ ${controller.getOrderedMenu[index].menu.price}',
                                  style: GoogleFonts.concertOne(
                                      color: ColorHelper.dark,
                                      fontWeight: FontWeight.w900,
                                      fontSize: 17)),
                              Text(' /items',
                                  style: TextStyle(
                                      color: ColorHelper.grey,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 17))
                            ],
                          ),
                          Row(
                            children: [
                              GestureDetector(
                                onTap: () {
                                  controller.removeOrderedMenu(
                                      controller.getOrderedMenu[index].menu);
                                },
                                child: Container(
                                  padding: const EdgeInsets.all(2),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: ColorHelper.secondary),
                                  child: Icon(Icons.remove,
                                      color: ColorHelper.primary),
                                ),
                              ),
                              Container(
                                padding: const EdgeInsets.all(3),
                                margin: EdgeInsets.only(
                                    left: Get.width * 0.02,
                                    right: Get.width * 0.02),
                                child: Text(
                                    controller.getOrderedMenu[index].quantity
                                        .toString(),
                                    style: TextStyle(
                                        color: ColorHelper.dark, fontSize: 22)),
                              ),
                              GestureDetector(
                                onTap: () {
                                  controller.addOrderedMenu(
                                      controller.getOrderedMenu[index].menu);
                                },
                                child: Container(
                                  padding: const EdgeInsets.all(2),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: ColorHelper.primary),
                                  child:
                                      Icon(Icons.add, color: ColorHelper.white),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ],
            ),
          );
        }),
        Container(
          margin: EdgeInsets.only(
              top: Get.height * 0.03, bottom: Get.height * 0.03),
          child: const AddNoteWidget(),
        ),
        OrderSummaryWidget(orderedMenus: controller.getOrderedMenu)
      ]);
    });
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<OrderController>(builder: (controller) {
      return controller.getOrderedMenu.isNotEmpty
          ? Container(
              padding: EdgeInsets.only(
                  left: Get.width * 0.03, right: Get.width * 0.03),
              child: orderList(),
            )
          : const EmptyOrderWidget();
    });
  }
}
